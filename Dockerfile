FROM centos

RUN useradd -ms /bin/bash appuser

USER appuser
WORKDIR /home/appuser

COPY index.html index.html

EXPOSE 8000

CMD python -m SimpleHTTPServer

